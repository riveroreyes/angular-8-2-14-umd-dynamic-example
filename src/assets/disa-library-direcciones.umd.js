(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('rxjs/operators'), require('rxjs'), require('disa-toast'), require('@angular/router'), require('@nebular/theme'), require('@angular/common'), require('disa-block-list'), require('@riveroreyes/optare-direcciones-edit'), require('@riveroreyes/optare-direcciones-new'), require('disa-input')) :
    typeof define === 'function' && define.amd ? define('disa-library-direcciones', ['exports', '@angular/core', '@angular/common/http', 'rxjs/operators', 'rxjs', 'disa-toast', '@angular/router', '@nebular/theme', '@angular/common', 'disa-block-list', '@riveroreyes/optare-direcciones-edit', '@riveroreyes/optare-direcciones-new', 'disa-input'], factory) :
    (global = global || self, factory(global['disa-library-direcciones'] = {}, global.ng.core, global.ng.common.http, global.rxjs.operators, global.rxjs, global.DisaToastModule, global.ng.router, global.NbLayoutModule, global.ng.common, global.DisaBlockListModule, global.OptareDireccionesEditModule, global.OptareDireccionesNewModule, global.DisaInputModule));
}(this, (function (exports, core, http, operators, rxjs, disaToast, router, theme, common, disaBlockList, optareDireccionesEdit, optareDireccionesNew, disaInput) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var DisaLibraryDireccionesService = /** @class */ (function () {
        function DisaLibraryDireccionesService() {
        }
        DisaLibraryDireccionesService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function DisaLibraryDireccionesService_Factory() { return new DisaLibraryDireccionesService(); }, token: DisaLibraryDireccionesService, providedIn: "root" });
        DisaLibraryDireccionesService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __metadata("design:paramtypes", [])
        ], DisaLibraryDireccionesService);
        return DisaLibraryDireccionesService;
    }());

    var config = {
        baseUrl: 'http://localhost:8081/',
        apiDatosFiscales: {
            apiGetall: 'base/query/consultaDatosFiscales',
            apiGetOne: 'base/query/datosFiscales',
            apiUpdate: 'base/action/modificarDatosFiscales',
            apiInsert: 'base/action/altaDatosFiscales',
            apiDelete: 'base/action/datosFiscales',
        },
        apiTipoDocumentos: {
            apiGetall: 'base/query/tipoDocumentos',
            apiGetOne: 'base/query/tipoDocumentos',
            apiUpdate: 'tipoDocumentos',
            apiInsert: 'tipoDocumentos',
            apiDelete: 'tipoDocumentos',
        },
        apiContactos: {
            apiGetall: 'base/query/consultaContactos',
            apiGetOne: 'base/query/contactos',
            apiUpdate: 'base/action/modificarContacto',
            apiInsert: 'base/action/altaContacto',
            apiDelete: 'contactos',
        },
        apiDirecciones: {
            apiGetall: 'base/query/consultaDirecciones',
            apiGetOne: 'base/query/direcciones',
            apiUpdate: 'base/action/modificarDireccion',
            apiInsert: 'base/action/altaDireccion',
            apiDelete: 'base/action/direccion',
        },
        apiProvincias: {
            apiGetall: 'base/query/provincias',
            apiGetOne: 'provincias',
            apiUpdate: 'provincias',
            apiInsert: 'provincias',
            apiDelete: 'provincias',
        },
        apiCiudades: {
            apiGetall: 'base/query/{idprovincia}/ciudades',
            apiGetOne: '',
            apiUpdate: '',
            apiInsert: '',
            apiDelete: '',
        },
        apiCalles: {
            apiGetall: 'base/query/{idCiudad}/calles',
            apiGetOne: '',
            apiUpdate: '',
            apiInsert: 'base/action/altaCalle',
            apiDelete: '',
        },
        apiTipoVias: {
            apiGetall: 'base/query/tipoVias',
            apiGetOne: '',
            apiUpdate: '',
            apiInsert: '',
            apiDelete: '',
        },
    };

    var DataDireccionesService = /** @class */ (function () {
        // messageChanged = new Subject<any>();
        function DataDireccionesService(httpClient, toastService) {
            this.httpClient = httpClient;
            this.toastService = toastService;
        }
        DataDireccionesService.prototype.getHeaders = function () {
            var headers = new http.HttpHeaders({
                'Content-Type': 'application/json',
                // tslint:disable-next-line: max-line-length
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcyMjIxMzU1LCJqdGkiOiJiZIxODdmZjAxMjE0MWEwOTUyMzM5YzEyNzFkODQ3MCIsInVzZXJfaWQiOjF9.FrKGJTY-C9I_2RAbXEXTI-8VhJR1-EOz1wvUMObpb0w',
            });
            return headers;
        };
        DataDireccionesService.prototype.notificar = function (message) {
            // this.messageChanged.next(message);
        };
        DataDireccionesService.prototype.showMessage = function (message) {
            this.addLight(message.titulo, message.contenido);
            // this.messageChanged.observers = [];
        };
        DataDireccionesService.prototype.addLight = function (titulo, contenido) {
            // this.toastService.addLightMessage(titulo, contenido);
        };
        DataDireccionesService.prototype.addSecondary = function (titulo, contenido) {
            // this.toastService.addSecondaryMessage(titulo, contenido);
        };
        // -------------------------DIRECCIONES----------------------
        DataDireccionesService.prototype.getDirecciones = function (params) {
            return this.httpClient.post(config.baseUrl + config.apiDirecciones.apiGetall, params, { headers: this.getHeaders() }).pipe(operators.map(function (response) {
                var key = 'data';
                return response[key];
            }), operators.catchError(function (error) {
                return rxjs.throwError('Fallo al leer las direcciones');
            }));
        };
        DataDireccionesService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: disaToast.DisaToastService }
        ]; };
        DataDireccionesService = __decorate([
            core.Injectable(),
            __metadata("design:paramtypes", [http.HttpClient,
                disaToast.DisaToastService])
        ], DataDireccionesService);
        return DataDireccionesService;
    }());

    var DisaLibraryDireccionesComponent = /** @class */ (function () {
        function DisaLibraryDireccionesComponent(dataService, router) {
            this.dataService = dataService;
            this.router = router;
            this.apiConfig = {
                baseUrl: config.baseUrl,
                headers: this.dataService.getHeaders(),
                api: config.apiDirecciones,
                dependenciasApi: [],
            };
        }
        DisaLibraryDireccionesComponent.prototype.ngOnInit = function () {
        };
        DisaLibraryDireccionesComponent.prototype.onNew = function (event) {
            this.router.navigate(['direcciones', 'new']);
            console.log(event);
        };
        DisaLibraryDireccionesComponent.prototype.onEdit = function (event) {
            console.log(event);
            this.router.navigate(['direcciones', event, 'edit']);
        };
        DisaLibraryDireccionesComponent.prototype.onShow = function (event) {
            console.log(event);
        };
        DisaLibraryDireccionesComponent.prototype.onDelete = function (event) {
            console.log(event);
        };
        DisaLibraryDireccionesComponent.ctorParameters = function () { return [
            { type: DataDireccionesService },
            { type: router.Router }
        ]; };
        DisaLibraryDireccionesComponent = __decorate([
            core.Component({
                selector: 'disashop-disa-library-direcciones',
                template: "\n  "
            }),
            __metadata("design:paramtypes", [DataDireccionesService, router.Router])
        ], DisaLibraryDireccionesComponent);
        return DisaLibraryDireccionesComponent;
    }());

    var MENU_ITEMS = [
        {
            title: 'Tablero',
            icon: 'home-outline',
            link: '/pages/dashboard',
            home: true,
        },
        {
            title: 'Funcionalidades',
            icon: 'edit-2-outline',
            children: [
                {
                    title: 'Modulo externo -direcciones',
                    link: '/disa-library-direcciones/content',
                },
            ],
        },
    ];

    var DisaLibraryDireccionesContentComponent = /** @class */ (function () {
        function DisaLibraryDireccionesContentComponent() {
            this.menu = MENU_ITEMS;
        }
        DisaLibraryDireccionesContentComponent.prototype.ngOnInit = function () {
        };
        DisaLibraryDireccionesContentComponent = __decorate([
            core.Component({
                selector: 'disashop-disa-library-direcciones-content',
                template: "<nb-layout windowMode>\n  <nb-layout-header fixed>\n    <disashop-disa-library-direcciones-header></disashop-disa-library-direcciones-header>\n  </nb-layout-header>\n\n  <nb-sidebar class=\"menu-sidebar\" tag=\"menu-sidebar\" responsive start>\n    <div class=\"scrollable\" style=\"padding: 40px 20px 0px 20px;\">\n      <nb-menu [items]=\"menu\"></nb-menu>\n    </div>\n  </nb-sidebar>\n\n  <nb-layout-column>\n    <router-outlet></router-outlet>\n  </nb-layout-column>\n\n  <nb-layout-footer fixed>\n    <ngx-footer></ngx-footer>\n  </nb-layout-footer>\n</nb-layout>\n\n<!--\n<ngx-one-column-layout>\n  <nb-menu [items]=\"menu\"></nb-menu>\n  <router-outlet></router-outlet>\n</ngx-one-column-layout>\n -->\n\n\n",
                styles: [""]
            }),
            __metadata("design:paramtypes", [])
        ], DisaLibraryDireccionesContentComponent);
        return DisaLibraryDireccionesContentComponent;
    }());

    var DireccionesListComponent = /** @class */ (function () {
        function DireccionesListComponent(dataDireccionesService, router, route) {
            this.dataDireccionesService = dataDireccionesService;
            this.router = router;
            this.route = route;
            this.EDIT_ITEM = 'EDIT_ITEM';
            this.SHOW_ITEM = 'SHOW_ITEM';
            this.DELETE_ITEM = 'DELETE_ITEM';
            this.items = [];
            this.pagination = 10;
            this.currentPage = 1;
            this.nombreProvincia = '';
            this.nombreCiudad = '';
            this.nombreCalle = '';
            this.codigoPostal = '';
            this.actions = [
                { label: 'Modificar', value: this.EDIT_ITEM, icon: 'far fa-edit text-warning' },
            ];
            this.titles = [
                { title: 'Provincia', class: 'col-6 col-md-2' },
                { title: 'Ciudad', class: 'col-6 col-md-2' },
                { title: 'Calle', class: 'col-6 col-md-2' },
                { title: 'Tipo vía', class: 'col-6 col-md-2' },
                { title: 'Código postal', class: 'col-6 col-md-2' },
                { title: 'Otros datos', class: 'col-6 col-md-2' },
            ];
        }
        DireccionesListComponent.prototype.ngOnInit = function () {
            // this.dataDireccionesService.messageChanged.subscribe((message: any) => this.dataDireccionesService.showMessage(message));
            this.getDatos();
        };
        DireccionesListComponent.prototype.getDatos = function () {
            var _this = this;
            var search = ({
                provincia: this.nombreProvincia,
                ciudad: this.nombreCiudad,
                calle: this.nombreCalle,
                codigoPostal: this.codigoPostal,
            });
            var params = __assign({ page: this.currentPage - 1, size: this.pagination, orderBy: '' }, search);
            this.dataDireccionesService.getDirecciones(params).subscribe(function (data) {
                var key = 'content';
                _this.items = data[key];
                key = 'totalElements';
                _this.totalItems = data[key];
                _this.initTable();
            });
        };
        DireccionesListComponent.prototype.initTable = function () {
            var _this = this;
            this.blockItems = [];
            if (this.items.length > 0) {
                this.items.forEach(function (item) {
                    var blockItem = new disaBlockList.DisaBlockItem();
                    blockItem.item = item;
                    blockItem.borderColor = 'lighgray';
                    blockItem.actions = _this.actions;
                    _this.blockItems.push(blockItem);
                });
            }
        };
        DireccionesListComponent.prototype.getAction = function (disaAction) {
            switch (disaAction.value) {
                case this.EDIT_ITEM:
                    return this.onEdit(disaAction);
                    break;
                case this.SHOW_ITEM:
                    return this.onShow(disaAction);
                    break;
                case this.DELETE_ITEM:
                    return this.onDelete(disaAction);
                    break;
                default:
                    break;
            }
        };
        DireccionesListComponent.prototype.getSelectedItem = function (event) {
        };
        DireccionesListComponent.prototype.pageChange = function (newPage) {
            this.currentPage = newPage;
            this.getDatos();
        };
        DireccionesListComponent.prototype.onSearch = function () {
            this.currentPage = 1;
            this.getDatos();
        };
        DireccionesListComponent.prototype.onNew = function () {
            this.router.navigate(['../', 'new'], { relativeTo: this.route });
        };
        DireccionesListComponent.prototype.onEdit = function (disaAction) {
            this.router.navigate(['../', disaAction.item.idDireccion, 'edit'], { relativeTo: this.route });
        };
        DireccionesListComponent.prototype.onShow = function (disaAction) {
            this.router.navigate(['../', disaAction.item.idDireccion], { relativeTo: this.route });
        };
        DireccionesListComponent.prototype.onDelete = function (disaAction) {
            var resp = confirm('¿Realmente desea borrar este item?');
            if (resp) {
                var pos = this.blockItems.findIndex(function (x) { return x.item === disaAction.item; });
                if (pos >= 0) {
                    this.blockItems.splice(pos, 1);
                }
            }
        };
        DireccionesListComponent.prototype.onKeypressChanged = function (event, model) {
            switch (model) {
                case 'nombreProvincia':
                    this.nombreProvincia = event;
                    break;
                case 'nombreCiudad':
                    this.nombreCiudad = event;
                    break;
                case 'nombreCalle':
                    this.nombreCalle = event;
                    break;
                case 'codigoPostal':
                    this.codigoPostal = event;
                    break;
                default:
                    break;
            }
        };
        DireccionesListComponent.ctorParameters = function () { return [
            { type: DataDireccionesService },
            { type: router.Router },
            { type: router.ActivatedRoute }
        ]; };
        DireccionesListComponent = __decorate([
            core.Component({
                // tslint:disable-next-line: component-selector
                selector: 'disa-library-direcciones-direcciones-list',
                template: "<div class=\"row d-flex justify-content-center\">\n  <div class=\"col-md-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        Direcciones\n      </div>\n      <div class=\"card-body\">\n        <div class=\"row justify-content-end\">\n          <div class=\"col-12 col-md-2 text-right\">\n            <button type=\"button\" class=\"btn btn-secondary btn-block\" (click)=\"onNew()\">\n              <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\n              Nuevo\n            </button>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-12 col-sm-12 col-md-8\">\n            <div class=\"row\">\n              <div class=\"col-12 col-sm-6 col-md-3\">\n                <div class=\"form-group\">\n                  <label for=\"\">Provincia: </label>\n                  <disashop-disa-input\n                  [varName]=\"nombreProvincia\"\n                  (keypressEmmiter)=\"onKeypressChanged($event, 'nombreProvincia')\"\n                ></disashop-disa-input>\n                </div>\n              </div>\n              <div class=\"col-12 col-sm-6 col-md-3\">\n                <div class=\"form-group\">\n                  <label for=\"\">Ciudad: </label>\n                  <disashop-disa-input\n                  [varName]=\"nombreCiudad\"\n                  (keypressEmmiter)=\"onKeypressChanged($event, 'nombreCiudad')\"\n                ></disashop-disa-input>\n                </div>\n              </div>\n              <div class=\"col-12 col-sm-6 col-md-3\">\n                <div class=\"form-group\">\n                  <label for=\"\">Calle: </label>\n                  <disashop-disa-input\n                  [varName]=\"nombreCalle\"\n                  (keypressEmmiter)=\"onKeypressChanged($event, 'nombreCalle')\"\n                ></disashop-disa-input>\n                </div>\n              </div>\n              <div class=\"col-12 col-sm-6 col-md-3\">\n                <div class=\"form-group\">\n                  <label for=\"\">C\u00F3digo postal: </label>\n                  <disashop-disa-input\n                  [varName]=\"codigoPostal\"\n                  (keypressEmmiter)=\"onKeypressChanged($event, 'codigoPostal')\"\n                ></disashop-disa-input>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12 col-sm-12 col-md-4\">\n            <div class=\"row\">\n              <div class=\"col-12 col-md-4 mt-4 mt-28px\">\n                <div class=\"form-group\">\n                  <button\n                    type=\"button\"\n                    class=\"btn btn-secondary btn-block\"\n                    (click)=\"onSearch()\"\n                  >\n                  <span class=\"d-none d-sm-block\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i> Buscar</span>\n                  <i class=\"fa fa-search d-block d-sm-none\" aria-hidden=\"true\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <!-- <div class=\"row\">\n          <div class=\"col-12 col-md-2\">\n            <div class=\"form-group\">\n              <label for=\"\">Calle: </label>\n              <input\n                type=\"text\"\n                class=\"form-control\"\n                id=\"nombreFiscal\"\n                [(ngModel)]=\"nombreProvincia\"\n              />\n            </div>\n          </div>\n          <div class=\"col-12 col-md-2\">\n            <div class=\"form-group\">\n              <label for=\"\">Ciudad: </label>\n              <input\n                type=\"text\"\n                class=\"form-control\"\n                id=\"numeroCiudad\"\n                [(ngModel)]=\"nombreCiudad\"\n              />\n            </div>\n          </div>\n          <div class=\"col-12 col-md-2\">\n            <div class=\"form-group\">\n              <label for=\"\">Tipo v\u00EDa: </label>\n              <input\n                type=\"text\"\n                class=\"form-control\"\n                id=\"numeroCalle\"\n                [(ngModel)]=\"nombreCalle\"\n              />\n            </div>\n          </div>\n          <div class=\"col-12 col-md-2\">\n            <div class=\"form-group\">\n              <label for=\"\">C\u00F3digo postal: </label>\n              <input\n                type=\"text\"\n                class=\"form-control\"\n                id=\"codigoPostal\"\n                [(ngModel)]=\"codigoPostal\"\n              />\n            </div>\n          </div>\n          <div class=\"col-12 col-md-2\">\n            <div class=\"form-group\">\n              <label for=\"\" style=\"visibility: hidden;\"\n                >LABEL INVISIBLE:\n              </label>\n              <button\n                type=\"button\"\n                class=\"btn btn-secondary btn-block\"\n                (click)=\"onSearch()\"\n              >\n                <i class=\"fa fa-search\" aria-hidden=\"true\"></i> Buscar\n              </button>\n            </div>\n          </div>\n        </div> -->\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <disashop-disa-block-list\n              [blockItems]=\"blockItems\"\n              [labelNoResults]=\"'No se encontraron resultados'\"\n              [previousLabel]=\"'Anterior'\"\n              [nextLabel]=\"'Siguiente'\"\n              [pagination]=\"pagination\"\n              [totalItems]=\"totalItems\"\n              [page]=\"currentPage\"\n              [idPagination]=\"'listing_pagination'\"\n              [type]=\"'table'\"\n              [showHeader]=\"false\"\n              [showActions]=\"true\"\n              [showInfoAdditional]=\"false\"\n              (actionSelected)=\"getAction($event)\"\n              (itemSelected)=\"getSelectedItem($event)\"\n              (pageSelected)=\"pageChange($event)\"\n            >\n              <!-- <ng-template #templateInfoAdditional let-item=\"item\">\n                  <span> {{ item.date | date: \"dd/mm/yyyy\" }} </span>\n                </ng-template> -->\n\n              <ng-template #templateDataHeader let-item=\"item\">\n                <div class=\"row mb-3\">\n                  <div [class]=\"field.class\" *ngFor=\"let field of titles\">\n                    <disashop-disa-block-data-label [label]=\"field.title\">\n                    </disashop-disa-block-data-label>\n                  </div>\n                </div>\n              </ng-template>\n\n              <!-- <ng-template #templateHeader let-item=\"item\" style=\"margin-bottom: 10px\">\n                  <span class=\"info-additional\">{{ item.client }}</span>\n                </ng-template> -->\n              <!-- CUSTOM DATA -->\n              <ng-template #templateData let-item=\"item\">\n                <div class=\"row\">\n                  <div class=\"col-6 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"item.calle.ciudad.areaAdministrativa2.nombre\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                  <div class=\"col-6 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"item.calle.ciudad.nombre\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                  <div class=\"col-6 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"item.calle.nombre\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                  <div class=\"col-6 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"item.calle.tipoVia.nombre\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                  <div class=\"col-6 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"item.codigoPostal\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                  <div class=\"col-12 col-md-2\">\n                    <disashop-disa-block-data-item\n                      [value]=\"'N\u00BA: '+item.numero + ' Piso: '+item.piso +  ' Puerta: ' + item.puerta\"\n                    >\n                    </disashop-disa-block-data-item>\n                  </div>\n                </div>\n              </ng-template>\n            </disashop-disa-block-list>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: [".info-additional{margin:auto;font-size:14px;font-weight:lighter;font-style:oblique}.card{border-radius:5px;border:1px solid #515a7b;font-family:'Open Sans Regular'}.card-header{background:#515a7b;color:#fff}input{height:25px}button{height:25px;font-size:12px;vertical-align:middle;padding:0 5px;background:#22243d}label{color:#000}"]
            }),
            __metadata("design:paramtypes", [DataDireccionesService, router.Router, router.ActivatedRoute])
        ], DireccionesListComponent);
        return DireccionesListComponent;
    }());

    var DireccionesEditComponent = /** @class */ (function () {
        function DireccionesEditComponent(dataDireccionesService, route, router) {
            this.dataDireccionesService = dataDireccionesService;
            this.route = route;
            this.router = router;
            this.apiConfig = {
                baseUrl: config.baseUrl,
                headers: this.dataDireccionesService.getHeaders(),
                api: config.apiDirecciones,
                dependenciasApi: [
                    { provincias: config.apiProvincias },
                    { ciudades: config.apiCiudades },
                    { calles: config.apiCalles },
                    { tipoVias: config.apiTipoVias },
                ],
            };
        }
        DireccionesEditComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.route.params.subscribe(function (params) {
                var key = 'id';
                _this.id = params[key];
            });
        };
        DireccionesEditComponent.prototype.onSave = function ($event) {
            this.router.navigate(['../../', 'list'], { relativeTo: this.route });
            // this.dataDireccionesService.notificar({titulo: 'Mensaje del servidor', contenido: 'Datos actualizados con exito'});
        };
        DireccionesEditComponent.prototype.onCancel = function ($event) {
            this.router.navigate(['../../', 'list'], { relativeTo: this.route });
        };
        //  INICIO VENTANA DE DETALLE MODAL
        DireccionesEditComponent.prototype.openDetailCalle = function (event) {
            this.showDetail = true;
        };
        DireccionesEditComponent.prototype.closeDetail = function () {
            this.showDetail = false;
        };
        DireccionesEditComponent.prototype.onSaveDetail = function ($event) {
            this.showDetail = false;
        };
        DireccionesEditComponent.prototype.onCancelDetail = function ($event) {
            this.showDetail = false;
        };
        DireccionesEditComponent.ctorParameters = function () { return [
            { type: DataDireccionesService },
            { type: router.ActivatedRoute },
            { type: router.Router }
        ]; };
        DireccionesEditComponent = __decorate([
            core.Component({
                selector: 'disashop-direcciones-edit',
                template: "<div class=\"container\">\n  <div class=\"row d-flex justify-content-center\">\n    <div class=\"col-md-12\" *ngIf=\"id\">\n      <disashop-optare-direcciones-edit-content\n        [id] = \"id\"\n        [title] = \"'Modificar direcci\u00F3n'\"\n        [apiConfig] = \"apiConfig\"\n        (saveEmmiter)=\"onSave($event)\"\n        (cancelEmmiter)=\"onCancel($event)\"\n        (callContactosEmmiter)=\"openDetailCalle($event)\"\n      ></disashop-optare-direcciones-edit-content>\n    </div>\n  </div>\n</div>\n",
                styles: [".ng-autocomplete{width:100%;display:table;margin:0 auto}::ng-deep .ng-autocomplete{height:25px;border-radius:5px;box-shadow:none}::ng-deep .ng-autocomplete .autocomplete-container{height:25px;border-radius:5px;box-shadow:none}::ng-deep .ng-autocomplete .autocomplete-container .input-container input{box-sizing:border-box;box-shadow:none;outline:0;background-color:#fff!important;color:rgba(0,0,0,.87);width:100%;padding:0 15px;line-height:40px;height:25px;border-radius:5px;border:1px solid silver}::ng-deep .ng-autocomplete .autocomplete-container .input-container .x i{color:rgba(0,0,0,.54);vertical-align:middle;font-size:12px}::ng-deep .ng-autocomplete .autocomplete-container .suggestions-container ul li a{padding:14px 15px;display:block;text-decoration:none;cursor:pointer;color:rgba(0,0,0,.87);font-size:10px!important}::ng-deep .ng-autocomplete .autocomplete-container .input-container input:disabled{background-color:#cecece!important}::ng-deep .autocomplete-container.active{z-index:1000!important}.overlay{z-index:9998;position:fixed;top:0;left:0;width:100%;height:100%;background-color:hsla(0,0%,100%,.6);cursor:default}"]
            }),
            __metadata("design:paramtypes", [DataDireccionesService,
                router.ActivatedRoute,
                router.Router])
        ], DireccionesEditComponent);
        return DireccionesEditComponent;
    }());

    var DireccionesNewComponent = /** @class */ (function () {
        function DireccionesNewComponent(dataDireccionesService, route, router) {
            this.dataDireccionesService = dataDireccionesService;
            this.route = route;
            this.router = router;
            this.apiConfig = {
                baseUrl: config.baseUrl,
                headers: this.dataDireccionesService.getHeaders(),
                api: config.apiDirecciones,
                dependenciasApi: [
                    { provincias: config.apiProvincias },
                    { ciudades: config.apiCiudades },
                    { calles: config.apiCalles },
                    { tipoVias: config.apiTipoVias },
                ],
            };
            this.apiConfigDetail = {
                baseUrl: config.baseUrl,
                headers: this.dataDireccionesService.getHeaders(),
                api: config.apiCalles,
                dependenciasApi: [
                    { provincias: config.apiProvincias },
                    { ciudades: config.apiCiudades },
                    { tipoVias: config.apiTipoVias },
                ],
            };
        }
        DireccionesNewComponent.prototype.ngOnInit = function () {
        };
        DireccionesNewComponent.prototype.onSave = function ($event) {
            this.router.navigate(['../', 'list'], { relativeTo: this.route });
        };
        DireccionesNewComponent.prototype.onCancel = function ($event) {
            this.router.navigate(['../', 'list'], { relativeTo: this.route });
        };
        //  INICIO VENTANA DE DETALLE MODAL
        DireccionesNewComponent.prototype.openDetailCalle = function (event) {
            this.showDetail = true;
        };
        DireccionesNewComponent.prototype.closeDetail = function () {
            this.showDetail = false;
        };
        DireccionesNewComponent.prototype.onSaveDetail = function ($event) {
            this.showDetail = false;
        };
        DireccionesNewComponent.prototype.onCancelDetail = function ($event) {
            this.showDetail = false;
        };
        DireccionesNewComponent.ctorParameters = function () { return [
            { type: DataDireccionesService },
            { type: router.ActivatedRoute },
            { type: router.Router }
        ]; };
        DireccionesNewComponent = __decorate([
            core.Component({
                selector: 'disashop-direcciones-new',
                template: "<div class=\"container\">\n  <div class=\"row d-flex justify-content-center\">\n    <div class=\"col-md-12\">\n      <disashop-optare-direcciones-new-content\n        [id] = \"null\"\n        [title] = \"'Nueva direcci\u00F3n'\"\n        [apiConfig] = \"apiConfig\"\n        (saveEmmiter)=\"onSave($event)\"\n        (cancelEmmiter)=\"onCancel($event)\"\n        (callContactosEmmiter)=\"openDetailCalle($event)\"\n      ></disashop-optare-direcciones-new-content>\n    </div>\n  </div>\n</div>\n\n",
                styles: [".ng-autocomplete{width:100%;display:table;margin:0 auto}::ng-deep .ng-autocomplete{height:25px;border-radius:5px;box-shadow:none}::ng-deep .ng-autocomplete .autocomplete-container{height:25px;border-radius:5px;box-shadow:none}::ng-deep .ng-autocomplete .autocomplete-container .input-container input{box-sizing:border-box;box-shadow:none;outline:0;background-color:#fff!important;color:rgba(0,0,0,.87);width:100%;padding:0 15px;line-height:40px;height:25px;border-radius:5px;border:1px solid silver}::ng-deep .ng-autocomplete .autocomplete-container .input-container .x i{color:rgba(0,0,0,.54);vertical-align:middle;font-size:12px}::ng-deep .ng-autocomplete .autocomplete-container .suggestions-container ul li a{padding:14px 15px;display:block;text-decoration:none;cursor:pointer;color:rgba(0,0,0,.87);font-size:10px!important}::ng-deep .ng-autocomplete .autocomplete-container .input-container input:disabled{background-color:#cecece!important}::ng-deep .autocomplete-container.active{z-index:1000!important}.overlay{z-index:9998;position:fixed;top:0;left:0;width:100%;height:100%;background-color:hsla(0,0%,100%,.6);cursor:default}"]
            }),
            __metadata("design:paramtypes", [DataDireccionesService,
                router.ActivatedRoute,
                router.Router])
        ], DireccionesNewComponent);
        return DireccionesNewComponent;
    }());

    var routes = [
        { path: 'content', pathMatch: 'full', redirectTo: 'content/list' },
        {
            path: 'content', component: DisaLibraryDireccionesContentComponent, children: [
                { path: 'list', component: DireccionesListComponent },
                { path: 'new', component: DireccionesNewComponent },
                { path: ':id/edit', component: DireccionesEditComponent },
            ],
        },
    ];
    var DisaLibraryDireccionesRoutingModule = /** @class */ (function () {
        function DisaLibraryDireccionesRoutingModule() {
        }
        DisaLibraryDireccionesRoutingModule = __decorate([
            core.NgModule({
                imports: [router.RouterModule.forChild(routes)],
                exports: [router.RouterModule],
            })
        ], DisaLibraryDireccionesRoutingModule);
        return DisaLibraryDireccionesRoutingModule;
    }());

    var DisaLibraryDireccionesHeaderComponent = /** @class */ (function () {
        function DisaLibraryDireccionesHeaderComponent(sidebarService, menuService, themeService, breakpointService) {
            this.sidebarService = sidebarService;
            this.menuService = menuService;
            this.themeService = themeService;
            this.breakpointService = breakpointService;
            this.userPictureOnly = false;
            this.currentTheme = 'default';
            this.userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
        }
        DisaLibraryDireccionesHeaderComponent.prototype.ngOnInit = function () {
            this.currentTheme = this.themeService.currentTheme;
            var xl = this.breakpointService.getBreakpointsMap().xl;
            this.sidebarService.collapse('menu-sidebar');
        };
        DisaLibraryDireccionesHeaderComponent.prototype.ngOnDestroy = function () {
        };
        DisaLibraryDireccionesHeaderComponent.prototype.toggleSidebar = function () {
            this.sidebarService.toggle(true, 'menu-sidebar');
            return false;
        };
        DisaLibraryDireccionesHeaderComponent.prototype.navigateHome = function () {
            this.menuService.navigateHome();
            return false;
        };
        DisaLibraryDireccionesHeaderComponent.ctorParameters = function () { return [
            { type: theme.NbSidebarService },
            { type: theme.NbMenuService },
            { type: theme.NbThemeService },
            { type: theme.NbMediaBreakpointsService }
        ]; };
        DisaLibraryDireccionesHeaderComponent = __decorate([
            core.Component({
                selector: 'disashop-disa-library-direcciones-header',
                template: "<div class=\"disa-header-container\">\n  <div class=\"disa-logo-container\">\n    <a (click)=\"toggleSidebar()\" href=\"#\" class=\"sidebar-toggle\">\n      <nb-icon icon=\"menu-2-outline\" class=\"nb-icono\"></nb-icon>\n    </a>\n    <a class=\"logo\" href=\"#\" (click)=\"navigateHome()\"><img src=\"assets/images/Logotipo-Disashop-RGB-small.png\" alt=\"\"></a>\n  </div>\n</div>\n\n<div class=\"disa-header-container\">\n  <nb-actions size=\"small\">\n\n    <!-- <nb-action class=\"control-item\">\n      <nb-search type=\"rotate-layout\"></nb-search>\n    </nb-action>\n    <nb-action class=\"control-item\" icon=\"email-outline\"></nb-action>\n    <nb-action class=\"control-item\" icon=\"bell-outline\"></nb-action>\n    <nb-action class=\"user-action\"  >\n      <nb-user [nbContextMenu]=\"userMenu\"\n               [onlyPicture]=\"userPictureOnly\"\n               [name]=\"user?.name\"\n               [picture]=\"user?.picture\">\n      </nb-user>\n    </nb-action> -->\n  </nb-actions>\n</div>\n",
                encapsulation: core.ViewEncapsulation.None,
                styles: ["disashop-disa-library-direcciones-header .disa-header-container{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;width:auto}disashop-disa-library-direcciones-header .disa-logo-container{display:-ms-grid;display:grid;-ms-grid-columns:40px 200px auto;grid-template-columns:40px 200px auto}disashop-disa-library-direcciones-header .nb-icono{font-size:1.25rem;line-height:1;width:2em;height:2em}"]
            }),
            __metadata("design:paramtypes", [theme.NbSidebarService,
                theme.NbMenuService,
                theme.NbThemeService,
                theme.NbMediaBreakpointsService])
        ], DisaLibraryDireccionesHeaderComponent);
        return DisaLibraryDireccionesHeaderComponent;
    }());

    var disashop = [
        optareDireccionesEdit.OptareDireccionesEditModule,
        optareDireccionesNew.OptareDireccionesNewModule,
    ];
    var generics = [
        disaBlockList.DisaBlockListModule,
        disaToast.DisaToastModule,
        disaInput.DisaInputModule,
    ];
    var template = [
        theme.NbLayoutModule,
        theme.NbMenuModule.forRoot(),
        theme.NbActionsModule,
        theme.NbIconModule,
        theme.NbUserModule,
    ];
    var DisaLibraryDireccionesModule = /** @class */ (function () {
        function DisaLibraryDireccionesModule() {
        }
        DisaLibraryDireccionesModule = __decorate([
            core.NgModule({
                declarations: [
                    DisaLibraryDireccionesComponent,
                    DisaLibraryDireccionesContentComponent,
                    DisaLibraryDireccionesHeaderComponent,
                    DireccionesListComponent,
                    DireccionesEditComponent,
                    DireccionesNewComponent,
                ],
                imports: __spread([
                    DisaLibraryDireccionesRoutingModule,
                    common.CommonModule
                ], generics, template, disashop),
                exports: [DisaLibraryDireccionesComponent],
                schemas: [core.CUSTOM_ELEMENTS_SCHEMA],
                providers: [DataDireccionesService, DisaLibraryDireccionesService],
            })
        ], DisaLibraryDireccionesModule);
        return DisaLibraryDireccionesModule;
    }());

    exports.DisaLibraryDireccionesComponent = DisaLibraryDireccionesComponent;
    exports.DisaLibraryDireccionesModule = DisaLibraryDireccionesModule;
    exports.DisaLibraryDireccionesService = DisaLibraryDireccionesService;
    exports.ɵa = DataDireccionesService;
    exports.ɵb = DisaLibraryDireccionesContentComponent;
    exports.ɵc = DisaLibraryDireccionesHeaderComponent;
    exports.ɵd = DireccionesListComponent;
    exports.ɵe = DireccionesEditComponent;
    exports.ɵf = DireccionesNewComponent;
    exports.ɵg = DisaLibraryDireccionesRoutingModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=disa-library-direcciones.umd.js.map
