import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { ModuleData } from '../models/module.model';
import { HttpClient } from '@angular/common/http';
import { Injectable, Compiler } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
// import { System } from 'systemjs';

import 'rxjs/add/operator/map';

// Needed for the new modules
import * as AngularCore from '@angular/core';
import * as AngularCommon from '@angular/common';
import * as AngularRouter from '@angular/router';
import * as HttpClientModule from '@angular/common/http';
import * as NebularThemeModule from '@nebular/theme';
import * as RxjsOperators from 'rxjs/operators';
import * as Rxjs from 'rxjs';
import * as DisaToastModule from 'disa-toast';
import * as DisaBlockListModule from 'disa-block-list';
import * as DisaInputModule from 'disa-input';
import * as OptareDireccionesEditModule from '@riveroreyes/optare-direcciones-edit';
import * as OptareDireccionesNewModule from '@riveroreyes/optare-direcciones-new';

declare var SystemJS: any;

@Injectable()
export class ModuleService {
  // source = `http://${window.location.host}/`;
  source = environment.sourceModule;
  constructor(private compiler: Compiler, private httpClient: HttpClient) {
  }

  loadModules(): Observable<ModuleData[]> {
    return this.httpClient.get('./assets/modules.json').pipe(
      map((modulesData: ModuleData[]) => {
        return modulesData;
      }),
      catchError((error: Response) => {
        return throwError('Fallo al leer los modulesData');
      },
      ),
    );
  }

  loadModule(moduleInfo: ModuleData): Observable<any> {
    const url = this.source + moduleInfo.location;
    return this.httpClient.get(url).pipe(
      map(source => {
        const exports = {}; // this will hold module exports
        /*const modules = {   // this is the list of modules accessible by plugins
          '@angular/core': AngularCore,
          '@angular/common': AngularCommon,
          '@angular/router': AngularRouter,
          '@riveroreyes/optare-direcciones-list': OptareDireccionesListModule,
          '@angular/common/http': HttpClientModule,
          '@nebular/theme': NebularThemeModule,
          'rxjs/operators': RxjsOperators,
          'rxjs': Rxjs,
        };*/

        // shim 'require' and eval
        // const require: any = (module) => modules[module];
        // tslint:disable-next-line: no-eval
        // tslint:disable-next-line: ban
        // eval(source.toString());

        // Need to check if there is another solution for eval as this is described as 'Evil'

        this.compiler.compileModuleAndAllComponentsSync(exports[`${moduleInfo.moduleName}`]);
        // console.log(exports); // disabled as this object is cleared anyway
        return exports;
      }),
    );
  }


  loadModuleSystemJS(moduleInfo: ModuleData): Promise<any> {
    // console.log(typeof SystemJS.set);
    const url = this.source + moduleInfo.location;
    //  console.log('URL DESDE LOADMODULESYSTEMJS', url);

    SystemJS.set('@angular/core', SystemJS.newModule(AngularCore));
    SystemJS.set('@angular/common', SystemJS.newModule(AngularCommon));
    SystemJS.set('@angular/router', SystemJS.newModule(AngularRouter));
    SystemJS.set('@angular/common/http', SystemJS.newModule(HttpClientModule));
    SystemJS.set('@nebular/theme', SystemJS.newModule(NebularThemeModule));
    SystemJS.set('rxjs/operators', SystemJS.newModule(RxjsOperators));
    SystemJS.set('rxjs', SystemJS.newModule(Rxjs));
    SystemJS.set('disa-toast', SystemJS.newModule(DisaToastModule));
    SystemJS.set('disa-block-list', SystemJS.newModule(DisaBlockListModule));
    SystemJS.set('disa-input', SystemJS.newModule(DisaInputModule));
    SystemJS.set('@riveroreyes/optare-direcciones-edit', SystemJS.newModule(OptareDireccionesEditModule));
    SystemJS.set('@riveroreyes/optare-direcciones-new', SystemJS.newModule(OptareDireccionesNewModule));

    // now, import the new module
    return SystemJS.import(`${url}`).then((module) => {
      // console.log('MODULE', module);
      // console.log('MODULO1', moduleInfo);

      return this.compiler.compileModuleAndAllComponentsAsync(module[`${moduleInfo.moduleName}`]).then(compiled => {
        // console.log('MODULO COMPILADO', compiled);
        return module;
      });
    });
  }

}
