import { Injectable } from '@angular/core';
import { config } from '../config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Message } from '../models/message.model';
import { DisaToastService } from 'disa-toast';
import { PreviaRuta } from '../interfaces/previa-ruta.interface';

@Injectable()
export class DataService {
  loading = true;
  messageChanged = new Subject<any>();
  previousRoute: PreviaRuta = {
    ruta: '',
    item: null,
    content: null,
    origen: '',
  };

  constructor(private httpClient: HttpClient, private toastService: DisaToastService) {
  }

  notificar(message: any): void {
    this.messageChanged.next(message);
  }

  showMessage(message: Message) {
    this.addLight(message.title, message.content);
    this.messageChanged.observers = [];
  }

  // ...Rutas previas
  setPreviousRoute(previousRoute: PreviaRuta) {
    this.previousRoute = previousRoute;
  }

  setPreviousRouteRuta(ruta: string) {
    this.previousRoute.ruta = ruta;
  }

  setPreviousRouteItem(item: any) {
    this.previousRoute.item = item;
  }

  setPreviousRouteContenido(content: any) {
    this.previousRoute.content = content;
  }

  getPreviousRoute(): PreviaRuta {
    return this.previousRoute;
  }

  addLight(title: string, content: string) {
    this.toastService.addLightMessage(title, content);
  }

  addSecondary(title: string, content: string) {
    this.toastService.addSecondaryMessage(title, content);
  }

  getHeaders() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line: max-line-length
      Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcyMjIxMzU1LCJqdGkiOiJiZIxODdmZjAxMjE0MWEwOTUyMzM5YzEyNzFkODQ3MCIsInVzZXJfaWQiOjF9.FrKGJTY-C9I_2RAbXEXTI-8VhJR1-EOz1wvUMObpb0w',
    });
    return headers;
  }

  getDatosFiscales(params: {}) {
    return this.httpClient.post(config.baseUrl + config.apiTaxData.apiGetall,
      params,
      { headers: this.getHeaders() }).pipe(
        map((response: any[]) => {
          const key = 'data';
          return response[key];
        }),
        catchError((error: Response) => {
          return throwError('Fallo al leer los datos fiscales');
        },
        ),
      );
  }
  getAddresses(params: {}) {
    return this.httpClient.post(config.baseUrl + config.apiAddresses.apiGetall,
      params,
      { headers: this.getHeaders() }).pipe(
        map((response: any[]) => {
          const key = 'data';
          return response[key];
        }),
        catchError((error: Response) => {
          return throwError('Fallo al leer las direcciones');
        },
        ),
      );
  }

  getContacts(params: {}) {
    this.loading = true;
    return this.httpClient.post(config.baseUrl + config.apiContacts.apiGetall,
      params,
      { headers: this.getHeaders() }).pipe(
        map((response: any[]) => {
          const key = 'data';
          this.loading = false;
          return response[key];
        }),
        catchError((error: Response) => {
          return throwError('Fallo al leer los contactos');
        },
        ),
      );
  }

  postAll(key: string, params: {}, id?: number) {
    const subKey = 'apiGetall';
    const apiGet = config[key];
    if (apiGet && apiGet[subKey]) {
      let url = config.baseUrl + apiGet[subKey];
      if (id) {
        const regex = /\{\w+\}/gi;
        url = url.replace(regex, id.toString());
      }
      return this.httpClient.post(url, params, { headers: this.getHeaders() }).pipe(
        map((response: any) => {
          const internKey = 'data';
          let informationData = response;
          if (response.hasOwnProperty(internKey)) {
            informationData = response[internKey];
          }
          return informationData;
        }),
        catchError((error: Response) => {
          return throwError('Failed to read all data from: ' + key);
        },
        ),
      );
    }
  }

}
