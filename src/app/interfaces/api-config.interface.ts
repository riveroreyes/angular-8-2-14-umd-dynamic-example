import { HttpHeaders } from '@angular/common/http';

export interface ApiConfig {
  baseUrl: string;
  headers: HttpHeaders;
  api: {
    apiGetall: string;
    apiGetOne: string;
    apiUpdate: string;
    apiInsert: string;
    apiDelete: string;
    adic01: string;
    adic02: string;
    adic03: string;
  };
  dependenciasApi: any[];
}
