import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'External modules',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'External module - Address',
        link: '/disa-library-direcciones/content',
      },
    ],
  },
];
