import { Component } from '@angular/core';

@Component({
  selector: 'ngx-form-elements',
  template: `
    <router-outlet></router-outlet>
    <ngx-my-loader> </ngx-my-loader>
  `,
})
export class FormsComponent {
}
