import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

interface User {
  email: string;
  password: string;
  rememberMe: boolean;
}


@Component({
  // tslint:disable-next-line: component-selector
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {

  user: User;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.router.navigate(['/pages']);
  }

  getConfigValue(value: any) {

  }

}
