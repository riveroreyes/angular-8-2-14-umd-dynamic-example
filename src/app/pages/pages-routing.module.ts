import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SpaHostComponent } from '../spa-host/spa-host.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'child1',
      children: [
        {
          path: '**',
          component: SpaHostComponent,
          data: { app: 'child1' },
        },
      ],
      data: { roles: ['client', 'admin'] },
    },
    {
      path: 'child2',
      children: [
        {
          path: '**',
          component: SpaHostComponent,
          data: { app: 'child2' },
        },
      ],
      data: { roles: ['client', 'admin'] },
    },
    {
      path: 'direcciones-ext',
      children: [
        {
          path: '**', pathMatch: 'full', redirectTo: '/disa-library-direcciones/content',
        },
      ],
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
