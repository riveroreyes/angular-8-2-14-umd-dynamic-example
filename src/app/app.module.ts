/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouteReuseStrategy } from '@angular/router';
import { MicroFrontendRouteReuseStrategy } from '../../src/services/route-reuse-strategy';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
// import { SpaHostComponent } from './spa-host/spa-host.component';
import { DataService } from './services/data-service.service';
import { PreviousRouteService } from './services/previous-route.service';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader-interceptor.service';

import { LoginComponent } from './pages/forms/login/login.component';
import { ForgotPasswordComponent } from './pages/forms/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/forms/reset-password/reset-password.component';
import { DisaToastModule } from 'disa-toast';
// import { MyLoaderComponent } from './my-loader/my-loader.component';
import { KeycloakService } from './keycloak.service';

export function kcFactory(keycloakService: KeycloakService): () => void {
  return () => keycloakService.init();
}

@NgModule({
  declarations: [AppComponent/*, SpaHostComponent*/, LoginComponent, ForgotPasswordComponent, ResetPasswordComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    ThemeModule.forRoot(),

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    DisaToastModule,
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: MicroFrontendRouteReuseStrategy,
    },
    DataService,
    PreviousRouteService,
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [],
})
export class AppModule {
}
