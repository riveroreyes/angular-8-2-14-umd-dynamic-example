import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbLogoutComponent,
  NbRegisterComponent,
} from '@nebular/auth';
import { LoginComponent } from './pages/forms/login/login.component';
import { ForgotPasswordComponent } from './pages/forms/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/forms/reset-password/reset-password.component';
// import { SpaHostComponent } from './spa-host/spa-host.component';

const entryComp = [/*SpaHostComponent*/];

const routes: Routes = [
  {
    path: 'pages',
    loadChildren: () => import('app/pages/pages.module')
      .then(m => m.PagesModule),
  },
  {
    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent/*NbLoginComponent*/,
      },
      {
        path: 'register',
        component: NbRegisterComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      {
        path: 'request-password',
        component: ForgotPasswordComponent,
      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
      },
    ],
  },
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  /* { path: '**', redirectTo: 'pages', pathMatch: 'full' }, */
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  entryComponents: entryComp,
})
export class AppRoutingModule {
}
