export class ResponseData {
  constructor(public content: any[], public totalElements: number) { }
}
